package com.b208.postApp.controllers;

import com.b208.postApp.models.Post;
import com.b208.postApp.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

//Handle all http responses
@RestController
//Enables cross origin resource requests
//CORS - cross origin resource sharing is the ability to allow or disallow applications to share resources from each other. With this, we can allow or disallow clients to access our API
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;

    //map web requests to controller methods via @RequestMapping
    //@RequestMapping(value="/posts", method = RequestMethod.POST)
    @PostMapping(value="/posts")
    public ResponseEntity<Object> createPost(@RequestHeader(value="Authorization") String stringToken, @RequestBody Post post){
        //System.out.println(post.getTitle());
        //System.out.println(post.getContent());

        postService.createPost(stringToken, post);

        return new ResponseEntity<>("Post Created Successfully", HttpStatus.CREATED);
    }

    @GetMapping(value="/posts")
    public ResponseEntity<Object> getPosts(){
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    @PutMapping("/posts/{id}")
    public ResponseEntity<Object> updatePost(@RequestHeader(value = "Authorization") String stringToken, @PathVariable Long id, @RequestBody Post post){
        //System.out.println(id);
        //System.out.println(post);

        return postService.updatePost(stringToken, id, post);
    }

    @DeleteMapping("/posts/{id}")
    public ResponseEntity<Object> deletePost(@RequestHeader(value = "Authorization") String stringToken, @PathVariable Long id){
        return postService.deletePost(stringToken, id);
    }
}
