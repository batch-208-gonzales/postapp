package com.b208.postApp.exceptions;

public class UserException extends Exception{
    public UserException(String message){
        super(message);
    }
}
