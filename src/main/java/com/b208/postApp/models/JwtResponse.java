package com.b208.postApp.models;

import java.io.Serializable;

public class JwtResponse implements Serializable {

    private static final long serialVersionUID = 5358178546291945874L;

    private final String jwttoken;

    public JwtResponse(String jwttoken) {
        this.jwttoken = jwttoken;
    }

    public String getToken() {
        return this.jwttoken;
    }
}
