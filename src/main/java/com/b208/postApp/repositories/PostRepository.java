package com.b208.postApp.repositories;

import com.b208.postApp.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

//an interface contains behaviors or methods that a class should contain
//an interface marked as @Repository means that the interface would contain or contains methods for database manipulation
//by extending CrudRepository, PostRepository has inherited pre-defined methods for creating, retrieving, updating and deleting records
@Repository
public interface PostRepository  extends CrudRepository<Post, Object> {
}
