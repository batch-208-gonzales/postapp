package com.b208.postApp.services;

import com.b208.postApp.models.User;
import com.b208.postApp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserRepository userRepository;

    public void createUser(User user) {
        userRepository.save(user);
    }

    public Optional<User> findByUsername(String username) {
        return Optional.ofNullable(userRepository.findByUsername(username));
    }

    public Iterable<User> getUsers() {
        return userRepository.findAll();
    }

    public void updateUser(Long id, User user) {
        User userForUpdating = userRepository.findById(id).get();
        userForUpdating.setUsername(user.getUsername());
        userForUpdating.setPassword(user.getPassword());

        userRepository.save(userForUpdating);
    }

    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }
}
