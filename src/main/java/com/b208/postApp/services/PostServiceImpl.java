package com.b208.postApp.services;

import com.b208.postApp.config.JwtToken;
import com.b208.postApp.models.Post;
import com.b208.postApp.models.User;
import com.b208.postApp.repositories.PostRepository;
import com.b208.postApp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

//springboot would recognize that business logic in this layer is implemented in this layer
@Service
public class PostServiceImpl implements PostService{
    //We're going to create an instance of the repository that is persistent within our springboot app
    //This is so we can use the pre-defined methods for database manipulation for our table
    @Autowired
    private PostRepository postRepository;

    @Autowired
    JwtToken jwtToken;

    @Autowired
    private UserRepository userRepository;

    public void createPost(String stringToken, Post post) {
        //Get the username from the token using jwtToken getUsernameFromToken method.
        //Then, pass the username
        //Get the user's detail from our table using our userRepository findByUsername method.
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        //Check if you can get the user from the table.
        //System.out.println(author.getUsername());
        Post newPost = new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);

        //When createPost is used from our services, we will be able to pass a Post class object and then, using the .save() method of our postRepository we will be able to insert a new row into our posts table
        postRepository.save(newPost);
    }

    public Iterable<Post> getPosts() {
        return postRepository.findAll();
    }

    public ResponseEntity updatePost(String stringToken, Long id, Post post) {
        Post postForUpdating = postRepository.findById(id).get();

        String postAuthor = postForUpdating.getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUser.equals(postAuthor)){
            postForUpdating.setTitle(post.getTitle());
            postForUpdating.setContent(post.getContent());

            postRepository.save(postForUpdating);

            return new ResponseEntity<>("Post Updated Successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Unauthorized Access", HttpStatus.UNAUTHORIZED);
        }
    }

    public ResponseEntity deletePost(String stringToken, Long id) {
        String postAuthor = postRepository.findById(id).get().getUser().getUsername();
        String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

        if(authenticatedUser.equals(postAuthor)){
            postRepository.deleteById(id);

            return new ResponseEntity<>("Post Deleted Successfully", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Unauthorized Access", HttpStatus.UNAUTHORIZED);
        }
    }
}
